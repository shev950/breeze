$(document).ready(function () {
    $('.item .js-basket-more').on('click', function(e) {
        e.preventDefault();
        //Инкремент суммы
        let quantityInput = $(this).parent().find('.count');
        let quantity = parseInt(quantityInput.val()) + 1;
        let sumValueElement = $(this).closest('tr').find('.sum .sum-value');
        let price = parseFloat(sumValueElement.data('price'));
        quantityInput.val(quantity);
        sumValueElement.text((price * quantity).toFixed(2));
        // Увеличение Total
        increaseTotal(1, price);
    });

    $('.item .js-basket-less').on('click', function(e) {
        e.preventDefault();
        //Декремент суммы
        let quantityInput = $(this).parent().find('.count');
        let quantity = parseInt(quantityInput.val()) - 1;
        if (quantity >= 1) {
            quantityInput.val(quantity);
            let sumValueElement = $(this).closest('tr').find('.sum .sum-value');
            let price = parseFloat(sumValueElement.data('price'));
            sumValueElement.text((price * quantity).toFixed(2));
            // Уменьшение Total
            reduceTotal(1, price);
        }
    });

    function getTotalElement() {
        return $('.section__prise .value');
    }

    // Увеличение total
    function increaseTotal(quantity, price) {
        let totalElement = getTotalElement();
        totalElement.text((parseFloat(totalElement.text()) + (quantity * price)).toFixed(2));
    }

    // Уменьшение total
    function reduceTotal(quantity, price) {
        let totalElement = getTotalElement();
        totalElement.text((parseFloat(totalElement.text()) - (quantity * price)).toFixed(2));
    }

    $('.remove').on('click', function (e) {
        e.preventDefault();
        $(this).closest('tr').remove();
        let price = parseFloat($(this).closest('tr').find('.sum .sum-value').data('price'));
        let quantity = parseFloat($(this).closest('tr').find('.item .count').val());
        reduceTotal(quantity, price);
    });
});