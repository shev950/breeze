$(document).ready(function () {

    $(".hamburger-nav").hide();
    $(".hamburger").click(function () {
        $(".hamburger-nav").slideToggle("slow", function () {
            $(".hamburger").hide();
            $(".cross").show();
        });
    });
    $(".cross").hide().click(function () {
        $(".hamburger-nav").slideToggle("slow", function () {
            $(".cross").hide();
            $(".hamburger").show();
        });
    });

    $('#lightSlider').lightSlider({
        item: 1,
        auto: true,
        speed: 900,
        pause: 5000,
        slideMargin: 0,
        pauseOnHover: true,
        loop: true
    });

    $('.productSlider').lightSlider({
        auto: false,
        loop: true,
        pager: false,
        item: 4,
        slideMove: 1,
        pauseOnHover: true,
        responsive: [
            {
                breakpoint: 993,
                settings: {
                    item: 3,
                    slideMove: 1,
                    slideMargin: 6,
                }
            },
            {
                breakpoint: 770,
                settings: {
                    item: 2,
                    slideMove: 1,
                    slideMargin: 6
                }
            },
            {
                breakpoint: 481,
                settings: {
                    item: 1,
                    slideMove: 1,
                    slideMargin: 6
                }
            }
        ]
    });

    $(".basket-link").fancybox();
});